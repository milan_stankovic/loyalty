package main

import (

	//"code.google.com/p/gcfg"
	"gopkg.in/gcfg.v1"
	"github.com/gin-gonic/gin"
	// "github.com/tommy351/gin-cors"
	"log"
	"loyalty/controller"
	_ "os"

)

type ConfigFile struct {
	Config struct {
		Port string
	}
	Hmac struct {
		Secret string
	}
}

func main() {

	var MyCfg ConfigFile

	log.Println("Loading config")
	//err := gcfg.ReadFileInto(&MyCfg, "config/loyalty/conf.gcfg")
	err := gcfg.ReadFileInto(&MyCfg, "/etc/ghub/config/loyalty/conf.gcfg")
	

	if err != nil {
		log.Panic(err)
		//os.Exit(1)
	}

	//controller.MockUpTier()

	router := gin.Default()
	// router.Use(cors.Middleware(cors.Options{
	// 	AllowOrigins:     []string{"*"},
	// 	AllowMethods:     []string{"PUT", "PATCH", "POST", "GET", "DELETE"},
	// 	AllowHeaders:     []string{"Origin", "Content-Type", "Authorization"},
	// 	ExposeHeaders:    []string{"Content-Length"},
	// 	AllowCredentials: true,
	// }))

	router.POST("loyalty/sendgift", controller.SendGift)
	router.POST("loyalty/redeemgift", controller.RedeemGift)
	router.POST("loyalty/deletegift", controller.DeleteGift)

	router.Run(":" + MyCfg.Config.Port)
}
