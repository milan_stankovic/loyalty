package controller

import (
	"encoding/json"
	"errors"
	"flag"
	"fmt"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	_ "gopkg.in/gcfg.v1"
	"github.com/garyburd/redigo/redis"
	"github.com/gin-gonic/gin"
	"log"
	_ "math"
	"net/http"
	"os"
	"time"

	//"tauth/auth"
	"teconomy/temodel"
	"tusers/tmusers"
)


const NANOSECONDS_IN_HOUR = 60 * 60 * 1000 * 1000 * 1000

var (
	MgoSession              	*mgo.Session
	Mongoserver             	= os.Getenv("MGOSERVER")
	Mogouser                	= os.Getenv("MGOUSER")
	Mogopass                	= os.Getenv("MGOPASS")

	TournamentsDB           	= "tournaments"
 	usercollection 				= "users"
 	tierscollection 			= "tiers"
 	gifthistorycollection 	= "gifthistory"
 	hmaccollection 				= "hmaccollection"

	RedisServer             	= os.Getenv("REDISSERVER")
	RedisPort               	= os.Getenv("REDISPORT")
	redisPool      redis.Pool
	redisAddress   = flag.String("redis-address", ":6378", "Address to the redis server")
	maxConnections = flag.Int("max-connections", 10, "Max connections to Redis")

)



type SendGiftBody struct {
	GiftType	string	`json:"giftType" bson:"giftType"`
	GsId 	string 	`json:"gsid" bson:"gsid"`
	ToFbId 	string 	`json:"tofbid" bson:"tofbid"`
	FbReqId string 	`json:"fbReqId" bson:"fbReqId"`
	Hmac 	string 	`json:"hmac" bson:"hmac"`
}


type RedeemGiftBody struct {
	GsId 		string 	`json:"gsid" bson:"gsid"`
	GsGiftId 	string 	`json:"gsGiftId" bson:"gsGiftId"`
	Hmac 		string 	`json:"hmac" bson:"hmac"`
}


type DeleteGiftBody struct {
	GsId 		string 	`json:"gsid" bson:"gsid"`
	GsGiftId 	string 	`json:"gsGiftId" bson:"gsGiftId"` 
	Hmac 		string 	`json:"hmac" bson:"hmac"`
}

type LoyaltyHistory struct  {
	Id 				bson.ObjectId    		`bson:"_id,omitempty"`
	Gift 			tmusers.ReceivedGift 	`bson:"gift"`
	TimeAction 		time.Time 				`bson:"time"`
	Action 			string 					`bson:"action"`
	Hmac 			string 	`json:"hmac" bson:"hmac"`
}



func init() {

	log.SetPrefix( ">>>>>>   " )


	if Mongoserver == "" {
		Mongoserver = "localhost:27017"
	}

	session, err := mgo.Dial( Mongoserver )
	if err != nil {
		log.Printf("database connection error")
		log.Fatal(err)
	}
	// economy.New()
	MgoSession = session


  	//maxWait := time.Duration(5 * time.Second)
    //session, sessionErr := mgo.DialWithTimeout("localhost:27017", maxWait)


	//MgoSession, _ = mgo.Dial("localhost:27017")
	// if err != nil {
	// 	panic(err)
	// }
	//defer MgoSession.Close()

	MgoSession.SetMode(mgo.Monotonic, true)



	if RedisServer == "" {
		RedisServer = "localhost"
	}
	if RedisPort == "" {
		RedisPort = "6378"
	}

}


// Sends gift from one user to another
func SendGift(c *gin.Context) {
	
	log.Println( "SendGift entered ..." )

	//userId, _, err := auth.GetReqUser( c.Request )

	// if err != nil {
	// 	log.Println( "Unauthorized: " + err.Error() )

	// 	c.JSON(http.StatusBadRequest, gin.H{"msg": "unauthorized"})
	// 	return
	// }

	request := SendGiftBody{}
	err := json.NewDecoder(c.Request.Body).Decode(&request)
	if err != nil {
		log.Println( "Decoding error: " + err.Error() )

		c.JSON(http.StatusBadRequest, gin.H{"msg": "bad request"})
		return
	}
	
	log.Println("")
	log.Println("giftType: " + request.GiftType)
	log.Println("gsid: " + request.GsId)
	log.Println("tofbid: " + request.ToFbId)
	log.Println("fbreqid: " + request.FbReqId)
	log.Println("hmac: " + request.Hmac)



	sess := MgoSession.Clone()
	defer sess.Close()
	sess.SetMode(mgo.Monotonic, true)
	userSender, userReceiver, hmac := tmusers.User{}, tmusers.User{}, tmusers.HmacSave{}



	if ok := bson.IsObjectIdHex( request.GsId ); !ok {
		log.Println("Requested id isn't userid ", request.GsId)
		c.JSON(http.StatusBadRequest, "error")
		return
	}

	log.Println( "GsId: " +  request.GsId )	

	userc := sess.DB(TournamentsDB).C( usercollection )
	tierc := sess.DB(TournamentsDB).C( tierscollection )
	hmacc := sess.DB(TournamentsDB).C( hmaccollection )

	err = hmacc.Find(bson.M{ "hmac": request.Hmac}).One( &hmac )
	if err != nil {
		log.Println( "Cannot find hmac: " + err.Error() )
		c.JSON( http.StatusBadRequest, "error" )
		return
	}

	err = userc.Find(bson.M{ "_id": bson.ObjectIdHex( request.GsId )}).One( &userSender )
	if err != nil {
		log.Println( "Cannot find user sender: " + err.Error() )
		c.JSON( http.StatusBadRequest, "error" )
		return
	}

	log.Println( "User sender found" )	



	if hmac.UserId != userSender.ID {
		log.Println( "hmac.UserId != userSender.Id: unauthorized" )
		c.JSON(http.StatusBadRequest, gin.H{"msg": "unauthorized"})
		return
	}

	err = userc.Find( bson.M{ "fbid": request.ToFbId }).One( &userReceiver )
	if err != nil {
		log.Println( "Cannot find user receiver: " + err.Error() )
		log.Println( err.Error() )
		c.JSON( http.StatusBadRequest, "error" )
		return
	}






	log.Println( "User receiver found" )

	if userSender.LoyaltyTierId == "" {
		log.Println( "Loyalty tier is not set for given user" )
		c.JSON( http.StatusBadRequest, "error" )
		return
	}

	if userSender.ID == userReceiver.ID {
		log.Println( "Cannot send gift to itself" )
		c.JSON( http.StatusBadRequest, "error" )
		return
	}



	tier := temodel.TierData{}
	err = tierc.Find(bson.M{ "_id": bson.ObjectIdHex( userSender.LoyaltyTierId )}).One( &tier )
	if err != nil {
		log.Println( "Cannot find tier: " + err.Error() )
		c.JSON( http.StatusBadRequest, "error" )
		return
	}
	log.Println( "Tier found" )


	// insert data for the first time

	//select appropriate gifting indicies for user and tier
	var giftingIndexUserSender int
	var giftingIndexUserReciever int
	var giftingIndexTier int
	for i,g := range userReceiver.Gifts {
		if g.GiftType == request.GiftType{
			giftingIndexUserReciever = i
		}
	}
	for i,g := range userSender.Gifts {
		if g.GiftType == request.GiftType{
			giftingIndexUserSender = i
		}
	}
	for i,g := range tier.Gifting {
		if g.GiftType == request.GiftType{
			giftingIndexTier = i
		}		
	}
	if userReceiver.Gifts[giftingIndexUserReciever].Exist == false {
		log.Println( "Has to add default values to receiver" )

//TODO: gifts array in mongo
		change := mgo.Change{
			Update: bson.M{	"$set": bson.M{"gifts.redeemPeriodReset": tier.Gifting[giftingIndexTier].RedeemPeriod, "gifts.redeemCountRemaining": tier.Gifting[giftingIndexTier].RedeemCount, "gifts.exist": true}},
		}
		_, err = userc.Find(bson.M{"_id": userReceiver.ID}).Apply(change, &userReceiver)
		if err != nil {
			log.Println( "Cannot update receiver for first time set data : " + err.Error() )
			c.JSON(http.StatusBadRequest, "error")
		}
	}


	//specific to coins gift type
	if request.GiftType == "coins"{

		coins := tier.Gifting[giftingIndexTier].Coins

		if userSender.Coins < int64( coins ) {
			log.Println( "User does not have enough coins for this gift" )
			c.JSON( http.StatusBadRequest, "error" )
			return
		}
	}


	// checks if sender didn't send in past couple hours to same user
	dur := time.Duration( int64( tier.Gifting[giftingIndexTier].ExpireDelay * NANOSECONDS_IN_HOUR ) )
	for _, sentGift := range userSender.Gifts[giftingIndexUserSender].SentGifts {
		if sentGift.FbId == userReceiver.Fbid && time.Now().Before( sentGift.SentTime.Add( dur ) ) {
			log.Println( "Sender should wait for expire time to send to same user" )
			c.JSON( http.StatusBadRequest, "error" )
			return
		}
	}

	sent, received := createGiftRecords( &userSender, &userReceiver, &tier, request.FbReqId )

//TODO: gifting array in mongo
	change := mgo.Change{
		Update: bson.M{	"$push": bson.M{"gifts.sent": sent},
						"$inc": bson.M{"coins": -coins}},
	}
	_, err = userc.Find(bson.M{"_id": userSender.ID}).Apply(change, &userSender)
	if err != nil {
		log.Println( "Cannot update sender: " + err.Error() )
		c.JSON(http.StatusBadRequest, "error")
	}


	change = mgo.Change{
		Update: bson.M{"$push": bson.M{"gifts.recv": received}},
	}
	_, err = userc.Find(bson.M{"_id": userReceiver.ID}).Apply(change, &userReceiver)
	if err != nil {
		log.Println( "Cannot update receiver: " + err.Error() )
		c.JSON(http.StatusBadRequest, "error")
	}

	log.Println( "SendGift ended ..." )
	
	c.String(http.StatusOK, ":OK")
}

// User redeems one of gifts
func RedeemGift(c *gin.Context) {

	log.Println( "RedeemGift entered ..." )
	

	request := RedeemGiftBody{}
	err := json.NewDecoder(c.Request.Body).Decode(&request)
	if err != nil {
		log.Println( "Decoding error: " + err.Error() )
		c.JSON(http.StatusBadRequest, gin.H{"msg": "bad request"})
		return
	}
	
	log.Println("")
	log.Println("gsid: " + request.GsId)
	log.Println("gsgiftid: " + request.GsGiftId)
	log.Println("hmac: " + request.Hmac)


	sess := MgoSession.Clone()
	defer sess.Close()
	sess.SetMode(mgo.Monotonic, true)
	userReceiver, hmac := tmusers.User{}, tmusers.HmacSave{}


	if ok := bson.IsObjectIdHex(request.GsId); !ok {
		log.Println("Requested id isn't userid ", request.GsId)
		c.JSON(http.StatusBadRequest, "error")
		return
	}

	userc := sess.DB(TournamentsDB).C(usercollection)
	hmacc := sess.DB(TournamentsDB).C( hmaccollection )


	err = hmacc.Find(bson.M{ "hmac": request.Hmac}).One( &hmac )
	if err != nil {
		log.Println( "Cannot find hmac: " + err.Error() )
		c.JSON( http.StatusBadRequest, "error" )
		return
	}


	err = userc.Find(bson.M{"_id": bson.ObjectIdHex( request.GsId )}).One(&userReceiver)
	if err != nil {
		log.Println( "Cannot find user receiver: " + err.Error() )
		c.JSON(http.StatusBadRequest, "error")
		return
	}

	log.Println( "User found" )

	if hmac.UserId != userReceiver.ID {
		log.Println( "hmac.UserId != userReceiver.Id: unauthorized" )
		c.JSON(http.StatusBadRequest, gin.H{"msg": "unauthorized"})
		return
	}



//TODO: wtf (should look for the gift id in all array elements)
	filter := func(s []tmusers.ReceivedGift, id string, fn func( gift tmusers.ReceivedGift, key string) bool) []tmusers.ReceivedGift {
	    var p []tmusers.ReceivedGift // == nil
	    for _, v := range s {
	        if fn(v, id) {
	            p = append(p, v)
	        }
	    }
	    return p
	}

	gifts := filter( userReceiver.Gifts.ReceivedGifts, request.GsGiftId, func( d tmusers.ReceivedGift, key string ) bool {
	 	return d.GsGiftId == key 
	}) 

	if len( gifts ) < 1 {
		log.Println( "Number of gifts is zero" )
		c.JSON(http.StatusBadRequest, "error")
		return
	}


	gift := gifts[0]

	log.Println( "Expires: " + gift.Expires.String() + " Now: " + time.Now().String() )

	if gift.Expires.Before( time.Now() ) {
		log.Println( "Gift has expired" )
		c.JSON(http.StatusBadRequest, "error")
		return
	}

	userSender := tmusers.User{}
	err = userc.Find(bson.M{"fbid": gift.FbId}).One( &userSender )
	if err != nil {
		log.Println( "Error in updating user sender: " + err.Error() )
		c.JSON( http.StatusBadRequest, "error" )
		return
	}


	// Checks if user has 
	if userReceiver.Gifts.RedeemCountRemaining < 1 {
		log.Println( "RedeemCount is less than 1" )
		c.JSON( http.StatusBadRequest, "error" )
		return
	}


//TODO: gifts as an array, no coins if wheel gift type 
	change := mgo.Change{
		Update:    bson.M{	"$pull": bson.M{"gifts.recv": bson.M{"_id": bson.ObjectIdHex(request.GsGiftId)}},
							"$inc": bson.M{"coins": gift.Coin, "gifts.redeemCountRemaining": -1}},
		ReturnNew: true,
	}
	_, err = userc.Find(bson.M{"_id": bson.ObjectIdHex( request.GsId )}).Apply(change, &userReceiver)
	if err != nil {
		log.Println( "Error in updating user receiver: " + err.Error() )
		c.JSON(http.StatusBadRequest, "error")
		return
	}


//TODO: gifts is an array
	change = mgo.Change{
		Update:    bson.M{"$pull": bson.M{"gifts.sent":  bson.M{"_id": bson.ObjectIdHex(request.GsGiftId)}}},
		ReturnNew: true,
	}
	_, err = userc.Find(bson.M{ "_id": userSender.ID }).Apply( change, &userSender )
	if err != nil {
		log.Println( "Error in updating user sender: " + err.Error() )
		c.JSON( http.StatusBadRequest, "error" )
		return
	}


	// send notification to user about new balance
	notifyMsg( userReceiver, gift.Coin )

	// store in history
	storeInHistory( sess, &gift, "redeem" )

	log.Println( "RedeemGift ended ..." )

	c.String( http.StatusOK, ":OK" )
}

// User deletes gift ( do not want to redeem )
func DeleteGift(c *gin.Context) {

	log.Println( "DeleteGift entered ..." )
	
	// userId, _, err := auth.GetReqUser( c.Request )

	// if err != nil {
	// 	log.Println( "Unauthorized: " + err.Error() )
	// 	c.JSON(http.StatusBadRequest, gin.H{"msg": "unauthorized"})
	// 	return
	// }


	request := DeleteGiftBody{}
	err := json.NewDecoder(c.Request.Body).Decode(&request)
	if err != nil {
		log.Println( "Decoding error: " + err.Error() )
		c.JSON(http.StatusBadRequest, gin.H{"msg": "bad request"})
		return
	}
	
	log.Println("")
	log.Println("gsid: " + request.GsId)
	log.Println("gsgiftid: " + request.GsGiftId)
	log.Println("hmac: " + request.Hmac)


	// if userId != request.GsId {
	// 	log.Println( "Ids are not the same")
	// 	c.JSON(http.StatusBadRequest, gin.H{"msg": "unauthorized"})
	// 	return	
	// }


	sess := MgoSession.Clone()
	defer sess.Close()
	sess.SetMode(mgo.Monotonic, true)
	userReceiver, hmac := tmusers.User{}, tmusers.HmacSave{}


	if ok := bson.IsObjectIdHex(request.GsId); !ok {
		log.Println("Requested id isn't userid ", request.GsId)
		c.JSON(http.StatusBadRequest, "error")
		return
	}

	userc := sess.DB(TournamentsDB).C(usercollection)
	hmacc := sess.DB(TournamentsDB).C( hmaccollection )


	err = hmacc.Find(bson.M{ "hmac": request.Hmac}).One( &hmac )
	if err != nil {
		log.Println( "Cannot find hmac: " + err.Error() )
		c.JSON( http.StatusBadRequest, "error" )
		return
	}


	err = userc.Find(bson.M{"_id": bson.ObjectIdHex( request.GsId )}).One(&userReceiver)
	if err != nil {
		log.Println( "Cannot find user receiver: " + err.Error() )
		c.JSON(http.StatusBadRequest, "error")
		return
	}

	log.Println( "User found" )

	if hmac.UserId != userReceiver.ID {
		log.Println( "hmac.UserId != userReceiver.Id: unauthorized" )
		c.JSON(http.StatusBadRequest, gin.H{"msg": "unauthorized"})
		return
	}


//TODO: anther wtf (should look for te giftid throughout the gifting array)
	filter := func(s []tmusers.ReceivedGift, id string, fn func( gift tmusers.ReceivedGift, key string) bool) []tmusers.ReceivedGift {
	    var p []tmusers.ReceivedGift // == nil
	    for _, v := range s {
	        if fn(v, id) {
	            p = append(p, v)
	        }
	    }
	    return p
	}

	gifts := filter( userReceiver.Gifts.ReceivedGifts, request.GsGiftId, func( d tmusers.ReceivedGift, key string ) bool {
	 	return d.GsGiftId == key 
	}) 

	if len( gifts ) < 1 {
		log.Println( "Number of gifts is zero" )
		c.JSON(http.StatusBadRequest, "error")
		return
	}


	gift := gifts[0]

//TODO: no coins update if giftType is wheel, gifts is now an array
	change := mgo.Change{
		Update:    bson.M{	"$pull": bson.M{"gifts.recv":  bson.M{"_id": bson.ObjectIdHex(request.GsGiftId)}}},
		ReturnNew: true,
	}
	_, err = userc.Find(bson.M{"_id": bson.ObjectIdHex( request.GsId )}).Apply(change, &userReceiver)
	if err != nil {
		log.Println( "Error in updating user receiver: " + err.Error() )
		c.JSON(http.StatusBadRequest, "error")
		return
	}

	userSender := tmusers.User{}
	err = userc.Find(bson.M{"fbid": gift.FbId}).One( &userSender )
	if err != nil {
		log.Println( "Cannot find user sender: " + err.Error() )
		c.JSON( http.StatusBadRequest, "error" )
		return
	}

//TODO: no coins update if giftType is wheel, gifts is now an array
	change = mgo.Change{
		Update:    bson.M{	"$pull": bson.M{"gifts.sent":  bson.M{"_id": bson.ObjectIdHex(request.GsGiftId)}},
							"$inc": bson.M{"coins": gift.Coin}},
		ReturnNew: true,
	}

	_, err = userc.Find(bson.M{"_id": userSender.ID}).Apply( change, &userSender )

	if err != nil {
		log.Println( "Error in updating user sender: " + err.Error() )
		c.JSON( http.StatusBadRequest, "error" )
		return
	}


	// store in history
	storeInHistory( sess, &gift, "delete" )

	log.Println( "DeleteGift ended ..." )

	c.String( http.StatusOK, ":OK" )
}


// Create records to put in database afetr sending gift to user
func createGiftRecords( sender *tmusers.User, receiver *tmusers.User, tier *temodel.TierData, fbReqId string ) ( *tmusers.SentGift, *tmusers.ReceivedGift) {

//TODO: sent gifts and received gifts are different depending on gift type
// tmusers.SentGiftCoins, tmusers.SentGiftWheel
// tmusers.RecievedGiftCoins, tmusers.RecievedGiftWheel
	sent, received := tmusers.SentGift{}, tmusers.ReceivedGift{}

	id := bson.NewObjectId()


	dur := time.Duration( int64( tier.Gifting.ExpireDelay * NANOSECONDS_IN_HOUR ) )
	expireTime := time.Now().Add( dur )

	sent.Id = id
	sent.GsGiftId = id.Hex()
	sent.FbId = receiver.Fbid
	sent.Coin = tier.Gifting.Coins
	sent.FbReqId = fbReqId
	sent.Expires = expireTime
	sent.SentTime = time.Now()



	received.Id = id
	received.GsGiftId = id.Hex()
	received.FbId = sender.Fbid
	received.Name = sender.Username
	received.Coin = tier.Gifting.Coins
	received.FbReqId = fbReqId
	received.Expires = expireTime


	log.Println( "Created records for db" )
	log.Println( sent )
	log.Println( received )


	return &sent, &received
}


// Sending notifiocation to user via redis / socket
func notifyMsg(user tmusers.User, coins int) {

	log.Println( "notifyMsg entered ..." )

	// ------------ sending redis balance notification ------------
	redisPool := redis.NewPool( func() ( redis.Conn, error ) {
		c, err := redis.Dial( "tcp", RedisServer + ":" + RedisPort )

		if err != nil {
			return nil, err
		}
		return c, err
	}, *maxConnections )

	defer redisPool.Close()
	redisConnect := redisPool.Get()

	notifyMsg := fmt.Sprintf( "%s!%d!%d!%d", user.ID.Hex(), user.Coins, coins, 0 )
	log.Println( "Send notification messages .. ", notifyMsg )
	if _, e := redisConnect.Do( "PUBLISH", "friendgift", notifyMsg ); e != nil {
		log.Println( "Error in publishing message ", e.Error() )
	}
	// ------------------------------------------------------------

	log.Println( "notifyMsg ended ..." )
}

// store in history collection 
//TODO: gift can be RecievedGiftCoins or RecievedGiftWheel
func storeInHistory( session *mgo.Session, gift *tmusers.ReceivedGift, action string )  error {

	log.Println( "MockUpTier entered ..." )

	if gift == nil {
		return errors.New( "Gift cannot be nil" )
	}

	
	sess := MgoSession.Clone()
	defer sess.Close()
	sess.SetMode(mgo.Monotonic, true)
	loyaltyhistoryc := sess.DB(TournamentsDB).C( gifthistorycollection )

	loyaltyHistory := LoyaltyHistory{}
	loyaltyHistory.Id = bson.NewObjectId()
	loyaltyHistory.Gift = *gift
	loyaltyHistory.TimeAction = time.Now()
	loyaltyHistory.Action = action


	// Insert Datas
	err := loyaltyhistoryc.Insert( &loyaltyHistory )

	if err != nil {
		log.Println( "Error in inserting loyaltyHistory in db" )
		return err
	}


	log.Println( "MockUpTier ended ..." )

	return nil
}


// Mockup function to insert some test tier in db, should not be used in production
func MockUpTier() {

	log.Println( "MockUpTier entered" )
	sess := MgoSession.Clone()
	defer sess.Close()
	sess.SetMode(mgo.Monotonic, true)
	tierc := sess.DB(TournamentsDB).C( tierscollection )

//TODO: gifting is now an array
	gifting := temodel.Gifting{}
	gifting.Coins 			= 100
	gifting.ExpireDelay 	= 1.0
	gifting.SendDelay 		= 1.0
	gifting.RedeemCount 	= 100
	gifting.RedeemPeriod 	= 1.0


	tier := temodel.TierData{}
	tier.ID = bson.NewObjectId()
	tier.Name = "Test"
	tier.Gifting = gifting

	// Insert Datas
	err := tierc.Insert( &tier )

	if err != nil {
		log.Println( "Error in inserting tier in db" )
		panic( err )
	}
}